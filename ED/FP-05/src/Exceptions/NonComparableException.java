package Exceptions;

public class NonComparableException extends Exception {

    public NonComparableException() {
        super();
    }

    public NonComparableException(String message) {
        super(message);
    }
}
