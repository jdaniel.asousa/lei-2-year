package List;


import Interfaces.OrderedListADT;
import Interfaces.UnorderedListADT;

public class Test {

    public static void main(String[] args) {

        OrderedListADT<String> myList = new DoubleLinkedOrderedList<>();

        UnorderedListADT<String> unorderedList = new DoubleLinkedUndorderedList<>();

        //--> Ordered List <--//
        int addTests = 4;
        int removeTests = 3;

        switch (addTests) {
            // Add in the front (empty list)
            case 1:
                myList.add("Test 1");
                System.out.println(myList.toString());

                break;

            // Add in front (with one element)
            case 2:
                myList.add("Test 1");
                myList.add("Test 0");
                System.out.println(myList.toString());

                break;

            // Add in rear
            case 3:
                myList.add("Test 1");
                myList.add("Test 0");
                myList.add("Test 2");
                System.out.println(myList.toString());

                break;

            // Add in middle
            case 4:
                myList.add("Test 1");
                myList.add("Test 0");
                myList.add("Test 3");

                System.out.println("List with Test 0, 1 and 3: \n" + myList.toString());

                myList.add("Test 2");
                System.out.println("List after add Test 2: \n" + myList.toString());

                break;
        }
        switch (removeTests) {
            // Remove in front
            case 1:
                myList.remove("Test 0");

                System.out.println("First after remove Test 0: \n" + myList.first());
                break;

            // Remove in middle
            case 2:
                myList.remove("Test 2");

                System.out.println("My list after remove Test 2 (in middle of list): \n" + myList.toString());
                break;

            // Remove in the rear
            // IMPORTANT after remove element appear in toString
            case 3:
                myList.remove("Test 3");

                System.out.println("My list after remove Test 3 (in rear of list): \n" + myList.toString());
                break;
        }

        //--> Unordered List <--//
        int unAddTests = 1;
        int unRemTests = 3;

        switch (unAddTests) {
            case 1:
                unorderedList.addToFront("Test 0");
                unorderedList.addToRear("Test 2");
                unorderedList.addAfter("Test 1", "Test 0");

                System.out.println(unorderedList.toString());

                System.out.println("First & Last: " + unorderedList.first() + ", " + unorderedList.last());
                break;
        }

        switch (unRemTests) {
            // Remove in Middle
            case 1:

                unorderedList.remove("Test 1");

                System.out.println("Last: " + unorderedList.last());
                System.out.println("My List after remove: \n" + unorderedList.toString());

                break;

            // Remove in Rear
            case 2:
                unorderedList.remove("Test 2");

                System.out.println("Last: " + unorderedList.last());
                System.out.println("My List after remove: \n" + unorderedList.toString());

                break;

            // Remove in Front
            case 3:
                System.out.println("First (before remove): " + unorderedList.first());

                unorderedList.remove("Test 0");

                System.out.println("First (after remove): " + unorderedList.first());
                System.out.println(unorderedList.toString());
        }
    }
}
