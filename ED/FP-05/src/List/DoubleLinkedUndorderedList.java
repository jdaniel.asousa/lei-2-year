package List;

import Interfaces.UnorderedListADT;
import Node.DoubleNode;

public class DoubleLinkedUndorderedList<T extends Comparable<T>> extends DoubleLinkedList<T> implements UnorderedListADT<T> {

    /**
     * Constructor Method to create new Linked List with front and rear equals to null
     */
    public DoubleLinkedUndorderedList() {
        super();
    }

    @Override
    public void addToFront(T element) {
        DoubleNode<T> nodeToAdd = new DoubleNode<>(element);

        if (this.isEmpty()) {
            this.front = this.rear = nodeToAdd;
        } else {

            nodeToAdd.setNext(this.front);
            this.front.setPrevious(nodeToAdd);

        }

        this.front = nodeToAdd;

        this.modcount++;
        this.count++;
    }

    @Override
    public void addToRear(T element) {
        DoubleNode<T> nodeToAdd = new DoubleNode<>(element);

        if (this.isEmpty()) {
            this.rear = this.front = nodeToAdd;
        } else {

            nodeToAdd.setPrevious(this.rear);
            this.rear.setNext(nodeToAdd);

        }

        this.rear = nodeToAdd;

        this.modcount++;
        this.count++;
    }

    @Override
    public void addAfter(T element, T target) {

        DoubleNode<T> nodeFinded = this.findElement(target);
        DoubleNode<T> nodeToAdd = new DoubleNode<>(element);

        if (this.isEmpty()) {

            this.front = this.rear = nodeToAdd;

        } else if (nodeFinded != null) {

            DoubleNode<T> nextNode = nodeFinded.getNext();

            nodeToAdd.setPrevious(nodeFinded);
            nodeToAdd.setNext(nextNode);

            nextNode.setPrevious(nodeToAdd);
            nodeFinded.setNext(nodeToAdd);

            this.modcount++;
            this.count++;

        }
    }
}
