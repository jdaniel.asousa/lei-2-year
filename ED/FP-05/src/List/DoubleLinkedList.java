package List;

import Interfaces.ListADT;
import Node.DoubleNode;

import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>> implements ListADT<T> {
    protected DoubleNode<T> front;
    protected DoubleNode<T> rear;

    // Number of alterations on list
    protected int modcount;

    protected int count;

    public DoubleLinkedList() {
        this.rear = null;
        this.front = null;
        this.count = 0;
        this.modcount = 0;
    }

    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }

    @Override
    public T remove(T element) {
        if (element instanceof Comparable) {
            // TODO: Exception
        }

        DoubleNode<T> nodeToRemove = this.findElement(element);

        if (this.isEmpty()) {
            return null;
        } else if (nodeToRemove != null) {
            if (nodeToRemove == this.front || this.size() == 1) {

                return this.removeFirst();

            } else if (nodeToRemove == this.rear) {

                return this.removeLast();

            } else {

                DoubleNode<T> nextNode = nodeToRemove.getNext();
                DoubleNode<T> previousNode = nodeToRemove.getPrevious();

                nextNode.setPrevious(previousNode);
                previousNode.setNext(nextNode);

                nodeToRemove.setPrevious(null);
                nodeToRemove.setNext(null);
            }

        }

        this.count--;
        this.modcount++;

        return nodeToRemove.getElement();
    }

    public DoubleNode<T> findElement(T element) {
        boolean found = false;

        DoubleNode<T> currentNode = this.front;
        DoubleNode<T> nodeToRemove = null;

        while (!found && currentNode != null) {

            if (currentNode.getElement().compareTo(element) >= 0) {
                found = true;
                nodeToRemove = currentNode;
            } else {
                currentNode = currentNode.getNext();
            }
        }

        return nodeToRemove;
    }

    @Override
    public boolean contains(T targetElement) {

        DoubleNode<T> currentNode = this.findElement(targetElement);

        return currentNode != null;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public Iterator<T> iterator() {
        return new DoubleLinkedIterator<>(this.modcount);
    }

    @Override
    public String toString() {

        StringBuilder listString = new StringBuilder();
        DoubleNode<T> currentNode = this.front;

        while (currentNode != null) {
            listString.append(currentNode.getElement().toString()).append("\n");
            currentNode = currentNode.getNext();
        }

        return listString.toString();
    }

    @Override
    public T first() {
        return this.front.getElement();
    }

    @Override
    public T last() {
        return this.rear.getElement();
    }

    @Override
    public T removeFirst() {

        if (this.isEmpty()) {
            return null;
        }

        DoubleNode<T> nodeToRemove = this.front;

        this.front = this.front.getNext();
        this.front.setPrevious(null);

        nodeToRemove.setNext(null);

        return nodeToRemove.getElement();
    }

    @Override
    public T removeLast() {
        if (this.isEmpty()) {
            return null;
        }

        DoubleNode<T> nodeToRemove = this.rear;

        this.rear = this.rear.getPrevious();
        this.rear.setNext(null);

        nodeToRemove.setPrevious(null);

        return nodeToRemove.getElement();
    }

    public class DoubleLinkedIterator<T> implements Iterator<T> {

        private DoubleNode<T> current;
        private int expected_modcount;
        private boolean can;

        public DoubleLinkedIterator(int modcount) {
            this.current = (DoubleNode<T>) DoubleLinkedList.this.front;
            this.can = false;
            this.expected_modcount = modcount;
        }

        @Override
        public boolean hasNext() {
            return (this.current.getNext() != null);
        }

        @Override
        public T next() {
            if (!this.hasNext()) {
                // TODO throw exception
            }

            T currentData = this.current.getElement();

            this.current = this.current.getNext();

            return currentData;
        }
    }
}
