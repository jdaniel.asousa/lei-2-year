package List;

import Interfaces.OrderedListADT;
import Node.DoubleNode;

public class DoubleLinkedOrderedList<T extends Comparable<T>> extends DoubleLinkedList<T> implements OrderedListADT<T> {


    /**
     * Constructor Method to create new Linked List with front and rear equals to null
     */
    public DoubleLinkedOrderedList() {
        super();
    }


    @Override
    public void add(T element) {
        if (element instanceof Comparable) {
            // TODO: Exception
        }

        DoubleNode<T> nodeToAdd = new DoubleNode<T>(element);

        if (this.isEmpty()) {

            this.front = this.rear = nodeToAdd;

        } else if (this.front.getElement().compareTo(element) >= 0) {

            // Insert element in head of list
            nodeToAdd.setNext(this.front);
            this.front.setPrevious(nodeToAdd);

            this.front = nodeToAdd;

        } else if (this.rear.getElement().compareTo(element) <= 0) {

            // Insert element in rear of list
            nodeToAdd.setPrevious(this.rear);
            this.rear.setNext(nodeToAdd);

            this.rear = nodeToAdd;

        } else {

            boolean found = false;
            DoubleNode<T> currentNode = this.front;

            while (!found && currentNode != null) {
                if (currentNode.getElement().compareTo(element) >= 0) {
                    found = true;
                } else {
                    currentNode = currentNode.getNext();
                }
            }

            nodeToAdd.setNext(currentNode);
            nodeToAdd.setPrevious(currentNode.getPrevious());

            currentNode.setPrevious(nodeToAdd);
            nodeToAdd.getPrevious().setNext(nodeToAdd);
        }

        this.modcount++;
        this.count++;
    }
}
