package Interfaces;

public interface UnorderedListADT<T> extends ListADT<T> {

    /**
     * Adds the specified element to this list
     *
     * @param element element to be added into this list
     */
    public void addToFront(T element);

    public void addToRear(T element);

    public void addAfter(T element, T target);
}