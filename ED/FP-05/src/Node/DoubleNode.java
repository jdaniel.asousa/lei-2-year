package Node;

public class DoubleNode<T> {

    private DoubleNode<T> next;
    private DoubleNode<T> previous;
    private T element;

    /**
     * Constructor method with node element and previous and next nodes;
     *
     * @param next     Next node
     * @param previous Previous Node
     * @param node     Node to set
     */
    public DoubleNode(DoubleNode<T> next, DoubleNode<T> previous, T node) {
        this.next = next;
        this.previous = previous;
        this.element = node;
    }

    /**
     * Constructor with just node element
     *
     * @param node Node to set
     */
    public DoubleNode(T node) {
        this.element = node;
        this.next = null;
        this.previous = null;
    }

    public DoubleNode<T> getNext() {
        return this.next;
    }

    public void setNext(DoubleNode<T> next) {
        this.next = next;
    }

    public DoubleNode<T> getPrevious() {
        return this.previous;
    }

    public void setPrevious(DoubleNode<T> previous) {
        this.previous = previous;
    }

    public T getElement() {
        return this.element;
    }
}
