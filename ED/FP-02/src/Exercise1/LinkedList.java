package Exercise1;

public class LinkedList<T> {

    /**
     * Variable to count number of elements in list
     */
    private int count = 0;

    private Node<T> head, tail;

    public LinkedList() {
        this.head = null;
        this.tail = null;
    }

    /**
     * Method to add node in list
     *
     * @param element Element to Add in List
     */
    public void add(T element) {

        Node<T> node = new Node<T>(element);

        if (this.head == null) {
            this.head = node;
        } else {
            node.setNext(head); //put my current head into the next position
            this.head = node;
        }

        this.count++;
    }

    /**
     * Remove element in the list
     *
     * @param element Element to Remove
     */
    public void remove(T element) {
        Node<T> current      = this.head;
        Node<T> previous     = null;
        boolean foundElement = false;

        while (!foundElement && current.getNext() != null) {
            if (current == element) {
                previous.setNext(current.getNext());
                foundElement = true;
            }

            previous = current;
            current.getNext();
        }
    }
}
