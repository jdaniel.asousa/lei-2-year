package main;

import examples.Pair;
import examples.TwoTypePair;
import examples.UnorderedPair;

import java.util.Collection;

public class main {
    private void printCollection(Collection<Object> c) {
        for (Object e : c) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {

        TwoTypePair<Integer, Integer> myPair    = new TwoTypePair<Integer, Integer>(24, 20);
        TwoTypePair<Integer, Integer> otherPair = new TwoTypePair<Integer, Integer>(24, 25);

        Pair<Integer> pair               = new Pair(10, 20);
        Pair<Integer> unorderedPair      = new UnorderedPair<Integer>(15, 20);
        Pair<Integer> otherUnorderedPair = new UnorderedPair<Integer>(18, 20);

        // Two Type Pair Tests
        System.out.println(myPair.toString());
        System.out.println("\nEquals: " + myPair.equals(otherPair));
        otherPair.setSecond(20);
        System.out.println("New second element in Other Pair (" + otherPair.getSecond() + ")" + "\n" + "Equals: " + myPair.equals(otherPair));

        // Pair Tests
        System.out.println("Max. element: " + pair.max());

        // Unorder Pair Tests
        System.out.println("\nUNORDERED PAIR TESTS \n");

        System.out.println("First Element: " + unorderedPair.getFirst());

        System.out.println("Unordered Pair Test Equals: " + unorderedPair.equals(otherUnorderedPair));

        Collection<Object> stones = (Collection<Object>) new Object();
    }


}
