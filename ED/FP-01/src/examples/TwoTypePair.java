package examples;

public class TwoTypePair<T1, T2> {

    private T1 first;
    private T2 second;

    public TwoTypePair() {
        first  = null;
        second = null;
    }

    /**
     * Constructor to set first and second item
     *
     * @param firstItem  T1 first item
     * @param secondItem T2 second item
     */
    public TwoTypePair(T1 firstItem, T2 secondItem) {
        first  = firstItem;
        second = secondItem;
    }

    public T1 getFirst() {
        return first;
    }

    public void setFirst(T1 first) {
        this.first = first;
    }

    public T2 getSecond() {
        return second;
    }

    public void setSecond(T2 second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return ("First Item: " + first.toString() + "\n" + "Second Item:" + second.toString());
    }

    @Override
    public boolean equals(Object otherObject) {
        if (otherObject == null || getClass() != otherObject.getClass()) {
            return false;
        } else {
            TwoTypePair otherPair = (TwoTypePair) otherObject;
            return (
                    first.equals(otherPair.first) && second.equals(otherPair.second)
            );
        }
    }
}
