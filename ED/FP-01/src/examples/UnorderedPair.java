package examples;

public class UnorderedPair<T extends Comparable> extends Pair<T> {

    public UnorderedPair() {
        super(null, null);
    }

    public UnorderedPair(T firstItem, T secondItem) {
        super(firstItem, secondItem);
    }


    @Override
    public T getFirst() {
        return super.getFirst();
    }

    @Override
    public T getSecond() {
        return super.getSecond();
    }

    @Override
    public void setFirst(T first) {
        super.setFirst(first);
    }

    @Override
    public void setSecond(T second) {
        super.setSecond(second);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (otherObject == null || getClass() != otherObject.getClass()) {
            return false;
        } else {
            UnorderedPair<T> otherPair = (UnorderedPair<T>) otherObject;

            return (
                    getFirst().equals(otherPair.getFirst()) && getSecond().equals(otherPair.getSecond()) || getFirst().equals(otherPair.getSecond()) && getSecond().equals(otherPair.getFirst())
            );
        }
    }
}
