package ArrayStack;

public class EmptyCollectionException extends Exception {
	public EmptyCollectionException(String error) {
		super(error);
	}
}
