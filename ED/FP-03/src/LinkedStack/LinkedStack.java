package LinkedStack;

import ArrayStack.EmptyCollectionException;
import ArrayStack.StackADT;

public class LinkedStack<T> implements StackADT<T> {
    private Node<T> top;

    private int count;

    /**
     * Constructor method to initialize the LinkedStack empty
     */
    public LinkedStack() {
        this.count = 0;
        this.top   = null;
    }

    /**
     * Method to add nodes
     *
     * @param element element to be pushed onto stack
     */
    @Override
    public void push(T element) {
        Node<T> newNode = new Node<T>(element);
        newNode.setNext(this.top);
        this.top = newNode;
        this.count++;
    }

    /**
     * Method to remove node
     *
     * @return node removed
     * @throws EmptyCollectionException
     */
    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }

        Node<T> tmpNode = this.top;
        this.top = this.top.getNext();
        tmpNode.setNext(null);

        this.count--;

        return (T) tmpNode.getElement();
    }

    /**
     * Method to return the top element
     *
     * @return T the top node
     * @throws EmptyCollectionException
     */
    @Override
    public T peek() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }

        return (T) this.top.getElement();
    }

    /**
     * Method to validate if the LinkedStack is empty or not
     *
     * @return true if stack is empty, or false if stack not empty
     */
    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }

    /**
     * Method to get size the Stack
     *
     * @return stack size
     */
    @Override
    public int size() {
        return this.count;
    }

    @Override
    public String toString() {

        StringBuilder strBuild = new StringBuilder();
        Node<T>       nodeTop  = top;

        while (nodeTop != null) {
            strBuild.append(nodeTop.getElement() + "\n");
            nodeTop = nodeTop.getNext();
        }
        return strBuild.toString();
    }
}
