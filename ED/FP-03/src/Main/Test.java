package Main;

import ArrayStack.ArrayStack;
import ArrayStack.EmptyCollectionException;
import ArrayStack.StackADT;
import LinkedStack.LinkedStack;

public class Test {

    public static void main(String[] args) {
        StackADT<String> stack     = new ArrayStack<String>();
        StackADT<String> stacKList = new LinkedStack<String>();

        try {

            System.out.println("Stack Empty? " + stack.isEmpty());
            stack.push("Test Stack");
            stack.push("Test Stack");

            System.out.println(stack.toString());
            System.out.println("Stack Size: " + stack.size());
            stack.pop();
            System.out.println("Stack Size before pop: " + stack.size());
            System.out.println(stack.toString());

            System.out.println("\nLINKED LIST");

            stacKList.push("Test LinkedStak");
            stacKList.push("Test LinkedStak 2");

            System.out.println("Before Remove: " + stacKList.size());
            stacKList.pop();
            System.out.println("After Remove:" + stacKList.size());
        } catch (EmptyCollectionException error) {
            System.out.println("Empty Collection: " + error);
        }
    }
}
