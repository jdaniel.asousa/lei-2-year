package LinkedStack;

public class Node<T> {

    private T       element;
    private Node<T> next;

    /**
     * Node Constructor
     *
     * @param element Element to Add in Node
     */
    public Node(T element) {
        this.element = element;
        this.next    = null;
    }

    /**
     * Get Next Node
     *
     * @return Node<T> The Next Node
     */
    public Node<T> getNext() {
        return this.next;
    }

    /**
     * Set Next Node
     *
     * @param element Next Node to Set
     */
    public void setNext(Node<T> element) {
        this.next = element;
    }

    public T getElement() {
        return this.element;
    }
}
