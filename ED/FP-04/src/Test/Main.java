package Test;

import Queue.CircularArray;
import Queue.QueueADT;

public class Main {
    public static void main(String[] args) {
        /*QueueADT<String> queue = new LinkedQueue<String>();

        try {
            System.out.println("NEW LINKED QUEUE");

            System.out.println("Empty: " + queue.isEmpty());
            queue.enqueue("Teste 1");
            queue.enqueue("Teste 2");

            System.out.println(queue.toString());

            System.out.println("\n--> DEQUEUE:");
            queue.dequeue();
            System.out.println(queue.toString());
            System.out.println("Actual Size: " + queue.size());
        } catch (EmptyCollectionException e) {
            System.out.println(e.toString());
        }*/

        QueueADT<String> circularArray = new CircularArray<String>(2);

        circularArray.enqueue("Test 1");
        circularArray.enqueue("Test 2");
        System.out.println("Circular Array size: " + circularArray.size());
        circularArray.enqueue("Test 3");

        System.out.println("Circular Array size (with Test 3): " + circularArray.size());
        circularArray.dequeue();
        System.out.println("Circular Array size (after Dequeue): " + circularArray.size());
    }
}
