package Queue;

import LinkedStack.Node;

public class LinkedQueue<T> implements QueueADT<T> {

    private int count;

    private Node<T> front, rear;

    public LinkedQueue() {
        this.count = 0;
        this.front = null;
        this.rear  = null;
    }

    /**
     * Adds one element to the rear of this queue. *
     *
     * @param element the element to be added to * the rear of this queue
     */
    @Override
    public void enqueue(T element) {
        Node<T> node = new Node<T>(element);

        if (isEmpty()) {
            this.front = node;

        } else {
            this.rear.setNext(node);
        }

        this.rear = node;
        this.count++;
    }

    /**
     * Removes and returns the element at the front of * this queue.
     *
     * @return the element at the front of this queue
     */
    @Override
    public T dequeue() {
        if (isEmpty()) {
            return null;
        }

        Node<T> tmpNode = this.front;
        this.front = this.front.getNext();

        this.count--;

        if (isEmpty()) {
            this.rear = null;
        }

        return (T) tmpNode.getElement();
    }

    /**
     * Returns without removing the element at the front of * this queue.
     *
     * @return the first element in this queue
     */
    @Override
    public T first() {
        return this.rear.getElement();
    }

    /**
     * Returns true if this queue contains no elements. *
     *
     * @return true if this queue is empty
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the number of elements in this queue.
     *
     * @return the integer representation of the size * of this queue
     */
    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder strBuild = new StringBuilder();
        Node<T>       current  = this.front;

        while (current != null) {
            strBuild.append(current.getElement());

            current = current.getNext();
        }

        return strBuild.toString();
    }
}
