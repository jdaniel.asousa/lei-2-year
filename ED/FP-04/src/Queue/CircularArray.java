package Queue;

public class CircularArray<T> implements QueueADT<T> {
    /**
     * constant to represent the default capacity of the array
     */
    private final int DEFAULT_CAPACITY = 100;

    /**
     * in that represents both the number of elements ant the next available
     * position in the array
     */
    private int front, rear;
    private int count = 0;

    /**
     * array of generic elements to represent the stack
     */
    private T[] queue;

    public CircularArray() {
        this.front = 0;
        this.rear = 0;
        this.queue = (T[]) (new Object[this.DEFAULT_CAPACITY]);
    }

    public CircularArray(int size) {
        this.front = 0;
        this.rear = 0;
        this.queue = (T[]) (new Object[size]);
    }

    /**
     * Adds one element to the rear of this queue. *
     *
     * @param element the element to be added to * the rear of this queue
     */
    @Override
    public void enqueue(T element) {

        if (size() == this.queue.length) {
            expandCapacity();
        }

        this.queue[this.rear] = element;

        this.rear = (this.rear + 1) % this.queue.length;

        this.count++;
    }

    /**
     * Removes and returns the element at the front of * this queue.
     *
     * @return the element at the front of this queue
     */
    @Override
    public T dequeue() {

        if (isEmpty()) {
            return null;
        }

        T elementToRemove = this.queue[this.front];
        this.queue[this.front] = null;
        this.front = (this.front + 1) % this.queue.length;

        this.count--;

        return elementToRemove;
    }

    /**
     * Returns without removing the element at the front of * this queue.
     *
     * @return the first element in this queue
     */
    @Override
    public T first() {
        return this.queue[this.front];
    }

    /**
     * Returns true if this queue contains no elements. *
     *
     * @return true if this queue is empty
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the number of elements in this queue.
     *
     * @return the integer representation of the size * of this queue
     */
    @Override
    public int size() {
        return this.count;
    }

    @Override
    public String toString() {
        StringBuilder strBuild = new StringBuilder();

        int actualFront = this.front;

        for (int i = actualFront; i < size(); i++) {
            strBuild.append(this.queue[i].toString());

            actualFront = (actualFront + 1) % this.queue.length;
        }

        return strBuild.toString();
    }

    private void expandCapacity() {
        T[] tempQueue = (T[]) (new Object[this.queue.length * 2]);

        int fTemp = front;

        for (int i = fTemp; i < size(); i++) {
            tempQueue[i] = this.queue[(this.rear + i) % this.queue.length];

            fTemp = (fTemp + 1) % this.queue.length;
        }

        this.queue = tempQueue;
        this.rear = 0;
        this.front = 0;
    }
}
