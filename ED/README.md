# Fichas desenvolvidas para a cadeira **Estrutura de Dados**

#### Softwares utilizados:
- IntelliJ IDEA <img src="http://resources.jetbrains.com/storage/products/intellij-idea/img/meta/intellij-idea_logo_300x300.png" width="50px" heigth="auto">

#### Fichas Realizadas
- [x] Ficha 1
- [x] Ficha 2
- [x] Ficha 3
- [x] Ficha 4
- [x] Ficha 5 *(only Double Linked List -  finish later)*