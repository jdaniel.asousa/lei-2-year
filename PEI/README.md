## Processamento Estruturado de Informação
<small><i>by <b>Daniel Sousa</b></i></small>

### Fichas
* [x] Ficha 01
* [x] Ficha 02
* [x] Ficha 03 (**@todo:** add namespaces in xml to validate)
* [ ] Ficha 04
* [ ] Ficha 05


### Software Utilizador
* **Visual Studio Code**: software utilizado apenas para criação de ficheiros XML
* **Oxygen XML**: software principal (para validar XSD, ...)

<img src="https://blog.launchdarkly.com/wp-content/uploads/2018/10/visualstudio_code-card.png" width="150px" heigth="auto">
<img src="https://i.pinimg.com/736x/94/44/0d/94440dc2b33d99dafd4bdb2967d68f76.jpg" width="150px" heigth="auto">
