package Ex3;

public class Monitor{
    protected int vez=-1;


	public synchronized void acordaTodas() {
		this.notifyAll();
	}
	
	public synchronized void espera() {
		try{
			this.wait();
		}catch (InterruptedException ie){}
	}
	

    public synchronized void defineVez(int vez){
        this.vez=vez;
    }

    public synchronized int leVez(){
        return this.vez;
    } 
}
