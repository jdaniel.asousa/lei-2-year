package Ex2;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.util.concurrent.*;

public class Janela implements Runnable {
	protected Monitor m;
	protected int i;

	public Janela (Monitor m, int i) {
		this.m=m;
		this.i=i;
	}	

    public void run() {
		String myname = Thread.currentThread().getName();
		JFrame f=new JFrame(myname);
		JLabel l=new JLabel("#");
		f.add(l);
		f.setSize(150,200);
		f.setLocation(i*200,100);
		f.setVisible(true);
        synchronized(m){
        try{
        m.wait();
        }
        catch(InterruptedException ie){}
}
        
		for(int i=0;i<20;i++) {
			try{
				Thread.sleep(100);
			}catch (InterruptedException ie){}
			l.setText(""+l.getText()+"#");
		} 
		f.dispose();
	}

    public static void main(String args[]) {
		Monitor mon = new Monitor();
		Thread[] ths = new Thread[8];

		
		for (int i=0; i<ths.length; i++) {
			ths[i] = new Thread(new Janela(mon,i),"Th"+i);
			ths[i].start();
		}
		System.out.println("[Main] All threads created!");
		System.out.println("[Main] Activting threads!");
        
        for(int i=0;i<ths.length;i++){
        try{
            Thread.sleep(1000);
        }
        catch (InterruptedException ie){}
        synchronized(mon){
        mon.notify();
        }

        }
		try{
			for (int i=0; i<ths.length; i++) {
				ths[i].join();
			}
		}catch (InterruptedException ie){}
		System.out.println("[Main] All threads ended!");	
	}	
	
}


		
