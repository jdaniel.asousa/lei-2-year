package Ex4;

public class Consumer extends Thread {

    private MyBuffer buffer;

    public Consumer(MyBuffer buff) {
        this.buffer = buff;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            synchronized (this.buffer) {
                while (!this.buffer.getFlag()) {
                    try {
                        this.buffer.wait();
                    } catch (InterruptedException ie) {
                    }
                }

                this.buffer.get();
                this.buffer.setFlag(false);
                this.buffer.notify();
            }
        }
    }

}
