package Ex4;

public class Exerc04 {

    public static void main(String[] args) {
        MyBuffer buffer = new MyBuffer();

        Consumer consumer = new Consumer(buffer);

        consumer.start();

        for (int i = 0; i < 10; i++) {
            synchronized (buffer) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ie) {
                }

                buffer.put((char) ('A' + i % 26));
                buffer.setFlag(true);
                buffer.notify();

                while (buffer.getFlag()) {
                    try {
                        buffer.wait();
                    } catch (InterruptedException ie) {
                    }
                }
            }
        }

        try {
            consumer.join();

        } catch (InterruptedException ie) {
        }

        System.out.println("All is done...");
    }

}
