package Ex4;

public class MyBuffer {

    private char buffer;
    private boolean flag = false;

    public synchronized void setFlag(boolean f) {
        flag = f;
    }

    public synchronized boolean getFlag() { return flag; }

    public synchronized void put(char c) {
        System.out.println("Putting " + c + "...");
        buffer = c;
    }

    public synchronized char get() {
        char c = buffer;

        System.out.println("Consumming " + c + "...");

        return c;
    }

}
